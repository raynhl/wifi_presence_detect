import time
import queue
import os
import threading
import json
from PortScannerModule.PortScannerModule import PortScanner
from ChatBotModule.ChatBotModule import ChatBot
from flask import Flask, render_template, request, jsonify, make_response

config = json.load(open("config.json"))

webui_module_folder_path = os.path.join(os.getcwd(), config["webui_folder"])
template_folder_path = os.path.join(webui_module_folder_path, "templates")
static_folder_path = os.path.join(webui_module_folder_path, "static")

app = Flask (config["app_name"], template_folder = template_folder_path, static_folder = static_folder_path)
current_devices = []
delta = []

###################################################
if config["proxy"]:
	import os
	os.environ["http_proxy"] = config["proxy"]
	os.environ["HTTP_PROXY"] = config["proxy"]
	os.environ["https_proxy"] = config["proxy"]
	os.environ["HTTP_PROXY"] = config["proxy"]

#################################################


@app.route("/")
def main_view():
	return render_template("web_view.html")

@app.route("/current_devices", methods=["GET"])
def get_current_devices():
	if request.method == "GET":
		if (len(current_devices) is not 0):
			data = {}
			data["current_devices"] = [device.serialize() for device in current_devices]
			
		else:
			data = {}
			data["current_devices"] = []
			
	response = jsonify(data = data)
	return response

@app.route("/delta", methods=["GET"])
def get_delta():
	if request.method == "GET":
		if (is_empty(delta)):
			data = {}
			data["added_devices"] = []
			data["dropped_devices"] = []
			data["status_changes"] = []
			data["id"] = delta["id"]
			
		else:
			data = {}
			data["added_devices"] = [device.serialize() for device in delta["added_devices"]]
			data["dropped_devices"] = [device.serialize() for device in delta["dropped_devices"]]
			data["status_changes"] = [device.serialize() for device in delta["status_changes"]]
			data["id"] = delta["id"]
			
	response = jsonify(data = data)
	return response


@app.route("/" + config["chatbot"]["token"] + "/updates", methods=["POST"])
def get_telegram_updates():
	data = request.get_json()
	on_chat_message_thread = threading.Thread(target=cb.on_chat_message, args = (data["message"], ))
	on_chat_message_thread.daemon = True
	on_chat_message_thread.start()
	return "OK"

def is_empty(delta):
	return not (delta["added_devices"] or delta["dropped_devices"] or delta["status_changes"])

def queue_listener():
	global current_devices
	global delta

	while(1):
		payload = webui_queue.get()

		if payload is None:
			print("[WEBUI] Sleeping...")
			time.sleep(1)

		else:
			current_devices = payload["devices"]
			delta = payload["delta"]

			i = 1
			for device in current_devices:
				if config["verbose"]: print("[WEBUI  ] [{:2}] {:15}\t{:17}\t{:10}\t{}".format(i, device.ip, device.mac, device.hostname, device.online))
				i += 1


if __name__ == "__main__":
	chatbot_queue = queue.Queue()
	webui_queue = queue.Queue()
	
	# Starting PortScanner module
	ps = PortScanner(config["subnet"], config["ip_address"], chatbot_queue, webui_queue, config["refresh_rate"], config["emulation_mode"])
	ps.daemon = True
	ps.start()

	# Starting Chatbot module
	cb = ChatBot(config["chatbot"], chatbot_queue)

	# Start queue listener for webui
	webui_queue_listener_thread = threading.Thread(target = queue_listener)
	webui_queue_listener_thread.daemon = True
	webui_queue_listener_thread.start()
	

	# Stating webui related functions
	app.run (
		port = config["port"],
		debug = True,
		) 

	# Should not reach
	while (True):
		print("WPD is running.")
		time.sleep(10)