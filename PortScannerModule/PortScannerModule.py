import time
import copy
import uuid
import nmap
from threading import Thread

def get_fake_devices(count):
	print("Generating fake devices")

	devices = []
	for i in range(0, count):
		devices.append(get_fake_device())

	return devices

def get_fake_device(ip_address = None):
	from faker import Faker
	fake = Faker()
	if ip_address is None:
		ip_address = fake.ipv4()

	return Device(ip_address, fake.mac_address(), fake.user_name()) 

def generate_fake_status(devices):
	from random import randint
	for device in devices:
		device.online = True if randint(0,1) else False

	return devices

name_dictionary = {"00:14:D1:B1:F7:A8": "Ray's Redmi",
					"": ""}


class PortScanner(Thread):
	def __init__(self,subnet_mask, ip_address, device_queue_1, device_queue_2, refresh_rate = 10, emulation = False):
		print("[INFO] Listening to device(s) on network.")
		Thread.__init__(self)
		self.refresh_rate = refresh_rate
		self.device_queue_1 = device_queue_1
		self.device_queue_2 = device_queue_2
		self.emulation = emulation
		if not self.emulation: self.nmap_scanner = NmapScanner(subnet_mask, ip_address)
		self.past_devices = []
		self.device_deltas = []


	def run(self):
		time.sleep(1)
		if self.emulation:
			self.current_devices = get_fake_devices(10)
		else:
			self.current_devices = self.nmap_scanner.get_devices()

		i = 0
		while(1):
			print("[REFRESH] Getting new device(s) list.")
			self.past_devices = copy.deepcopy(self.current_devices)

			# Emulation mode
			if self.emulation:
				if (i % 5) == 0:
					self.current_devices.append(get_fake_device())

				if (i % 8) == 0:
					self.current_devices = self.current_devices[:-2]

				self.current_devices = generate_fake_status(self.current_devices)
				self.current_devices.append(get_fake_device())

			else:
				self.current_devices = self.nmap_scanner.get_devices() 

			self.device_deltas = self.get_devices_delta()  
			
			payload = self.generate_payload(self.current_devices, self.device_deltas)

			self.device_queue_1.put(payload)
			self.device_queue_2.put(payload)

			time.sleep(self.refresh_rate)
			i+=1

	def get_devices_delta(self):

		added_devices = []
		dropped_devices = []
		status_changed_devices = []

		current_devices_ip = [device.ip for device in self.current_devices]
		past_devices_ip = [device.ip for device in self.past_devices]

		added_devices_ip = list(set(current_devices_ip) - set(past_devices_ip))
		if added_devices_ip:
			added_devices = [copy.deepcopy(self.current_devices[self.get_index_of(self.current_devices, device)]) for device in added_devices_ip]

		dropped_devices_ip = list(set(past_devices_ip) - set(current_devices_ip))
		if dropped_devices_ip:
			dropped_devices = [copy.deepcopy(self.past_devices[self.get_index_of(self.past_devices,device)]) for device in dropped_devices_ip]


		mutual_devices_ip = list(set(past_devices_ip) & set(current_devices_ip))

		for device in mutual_devices_ip:
			status_1 = self.current_devices[self.get_index_of(self.current_devices, device)].online
			status_2 = self.past_devices[self.get_index_of(self.past_devices,device)].online 

			if status_1 != status_2 :
				status_changed_devices.append(copy.deepcopy(self.current_devices[self.get_index_of(self.current_devices,device)])) 

		packet_uuid = str(uuid.uuid4())

		delta = {
				"added_devices": added_devices,
				"dropped_devices": dropped_devices,
				"status_changes": status_changed_devices,
				"id": packet_uuid
				}
		
		return delta

	def get_index_of(self, devices, key):
		for index, device in enumerate(devices):
			if device.ip == key:
				return index

		return -1

	def generate_payload(self, current_devices, delta):
		payload = {"devices": current_devices,
					"delta": delta
				}

		return payload

class NmapScanner():
	def __init__(self, subnet_mask, ip_address):
		self.subnet_mask = subnet_mask
		self.ip_address = ip_address
		print("[INFO] Listening to device(s) on network.")

	def get_devices(self):
		print("[NMAP Scanner] Scanning for devices.")
		ip_address = self.ip_address + "/24"
		devices =[]

		nm = nmap.PortScanner()
		nm.scan(ip_address, arguments='-sP')
		
		for host in nm.all_hosts():
			print(host)
			print ("STATUS:", nm[host]['status']['state'])
			#Print the MAC address
			try:
				ipv4 = nm[host]['addresses']['ipv4']
				mac = nm[host]['addresses']['mac']

				try:
					hostname = name_dictionary[mac]
				except KeyError:
					hostname = "unknownUser"

				print("IP Addresses: ",ipv4)
				print ("MAC ADDRESS:", mac)
				print("Hostname: ", hostname)
				devices.append(Device(ipv4,mac,hostname))
			
			except:
				mac = 'unknown'

		#Append some additional devices:
		devices.append(get_fake_device("192.168.5.1"))
		devices.append(get_fake_device("192.168.5.4"))

		del nm
		return devices

class Device():
	def __init__(self, ip_addr, mac_addr, hostname):
		self.ip = ip_addr
		self.mac = mac_addr
		self.hostname = hostname
		self.num_detected_offline = 0
		self.online = True

	def serialize(self):
		return {
			"ip": self.ip,
			"mac": self.mac,
			"hostname": self.hostname,
			"num_detected_offline": self.num_detected_offline,
			"online":self.online
		}
